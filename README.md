# XReloadPlugins

## Overview

X-Plane plugin to reload all plugins without restarting the simulator (currently Linux only).

## Description

The plugin adds a new submenu once you start a new flight in X-Plane called "XReloadPlugins".
If you click on the "Reload all plugins" menu item, X-Plane will stop the simulation and show a dialog, asking for confirmation to reload all plugins.
Once the user confirms, all plugins are reloaded and the simulation resumes.

If plugins are removed or added from the X-Plane plugins folder below `Resouces/plugins`, then they are also properly updated.

This plugin is useful if you install or change plugins, and you don't want to wait for a full restart of X-Plane.
It will also speed up the testing cycle if you are a plugin developer.

XReloadPlugins is very lightweight and will only consume a tiny bit of memory and almost no CPU cycles.

## Requirements

XReloadPlugins was tested with the Steam version of X-Plane 11.53, but is expected to work with other versions too.  
Currently the only supported operating system is Linux.

## Usage

Download the latest plugin archive here: [xreloadplugins_latest.tar.gz](http://www.callistix.net/releases/xreloadplugins/xreloadplugins_latest.tar.gz).

Extract the archive inside the `Resouces/plugins` directory of your X-Plane installation.  
Start X-Plane and the plugin will appear in the menu, once a flight is started.

The plugin writes it's logs to the main X-Plane log file, you can grep for the plugin name to troubleshoot it, e.g.:

    $ grep -i xreloadplugins Log.txt
    Loaded: /home/vitaminx/.steam/debian-installation/steamapps/common/X-Plane 11/Resources/plugins/xreloadplugins/64/lin.xpl (net.callistix.xreloadplugins).

## Compilation

### Dependencies

XReloadPlugins needs following components to compile:

- a C compiler like gcc
- standard C headers
- X-Plane SDK

I.e. on Debian or Ubuntu you would install the compiler and headers like that:

    sudo apt-get install build-essentials

Then download the X-Plane SDK from [Plugin SDK Downloads](https://developer.x-plane.com/sdk/plugin-sdk-downloads/) and extract its content into the `xreloadplugins/code` directory.  
It should look something like this:

    ~/xreloadplugins/code$ ll
    total 228K
    drwxr-xr-x 2 vitaminx vitaminx 4.0K Apr  4 15:41 64
    drwxr-xr-x 5 vitaminx vitaminx 4.0K Apr  4 16:38 SDK
    -rw-r--r-- 1 vitaminx vitaminx  893 Apr  4 15:40 main.c
    -rw-r--r-- 1 vitaminx vitaminx  252 Apr  4 16:05 main.h
    -rw-r--r-- 1 vitaminx vitaminx  67K Apr  4 15:41 main.o
    -rw-r--r-- 1 vitaminx vitaminx  545 Apr  4 15:37 Makefile
    [...]

### Compiling

Run `make` to compile the plugin:

    ~/xreloadplugins/code$ make
    cc -g3 -DDEBUG -Wall -DLIN -DXPLM200 -DXPLM210 -DXPLM300 -DXPLM301 -DXPLM302 -DXPLM303 -DAPL=0 -DIBM=0 -DLIN=1 -ISDK/CHeaders/XPLM -ISDK -fvisibility=hidden -c main.c
    cc -g3 -DDEBUG -Wall -DLIN -DXPLM200 -DXPLM210 -DXPLM300 -DXPLM301 -DXPLM302 -DXPLM303 -DAPL=0 -DIBM=0 -DLIN=1 -ISDK/CHeaders/XPLM -ISDK -fvisibility=hidden -c menu.c
    cc -g3 -DDEBUG -Wall -DLIN -DXPLM200 -DXPLM210 -DXPLM300 -DXPLM301 -DXPLM302 -DXPLM303 -DAPL=0 -DIBM=0 -DLIN=1 -ISDK/CHeaders/XPLM -ISDK -fvisibility=hidden -c reload.c
    cc -shared main.o menu.o reload.o -o 64/lin.xpl -lm
    chmod 644 64/lin.xpl

The plugin should then appear inside the `64` directory, it's called `lin.xpl`.

## Links

- direct link to latest release: http://www.callistix.net/releases/xreloadplugins/xreloadplugins_latest.tar.gz
- all releases: https://gitlab.com/callistix1/xreloadplugins/-/releases
- GitLab project: https://gitlab.com/callistix1/xreloadplugins
- README file: https://gitlab.com/callistix1/xreloadplugins/-/blob/master/README.md

## Todo

- Windows and Mac OS X version
- check if new plugins are recognized correctly on Windows platform, see [forums.x-plane.org](https://forums.x-plane.org/index.php?/files/file/71226-xreloadplugins/&do=findComment&comment=335801)

## Author and Contact

To report any issues or for suggestions contact me here or drop a mail to "vitaminx /at/ callistix.net".

## License

This software is released under the GNU General Public License Version 3.  
Please see the file COPYING for a copy of the license.
