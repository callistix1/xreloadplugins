/* menu.h */

/*  This file is part of XReloadPlugins.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef _MENU_H
#define _MENU_H

#include "XPLMMenus.h"

// visible names of main menu and menu items
#define MENU_NAME_MAIN      "XReloadPlugins"
#define MENU_NAME_RELOAD    "Reload all plugins"

// struct to hold all menu data
struct s_menu {
    int container_id;
    XPLMMenuID id;
    int settings_id;
};

// menu related prototypes
int menu_init(void);
void menu_destroy(void);
void menu_handler(void *in_menu_ref, void *in_item_ref);

#endif /* _MENU_H */
