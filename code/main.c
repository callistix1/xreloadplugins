/* main.c */

/*  This file is part of XReloadPlugins.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <string.h>

#include "main.h"
#include "menu.h"


PLUGIN_API int XPluginStart(char *outName, char *outSig, char *outDesc) {
    char err_str[255];
    int rc;

    // register plugin details with X-Plane
    strcpy(outName, PLUGIN_NAME);
    strcpy(outSig, PLUGIN_SIG);
    strcpy(outDesc, PLUGIN_DESC);

    // create menu, print log message and exit if an error was returned
    if ((rc = menu_init()) != 0) {
        snprintf(err_str, 255, "%s: could not initialize menu, code %i.\n", PLUGIN_NAME, rc);
        XPLMDebugString(err_str);
        return 0;
    }

    return 1;
}

PLUGIN_API int XPluginEnable(void) {
    return 1;
}

PLUGIN_API void XPluginDisable(void) {
}

PLUGIN_API void XPluginStop(void) {
    // destroy the menu
    menu_destroy();
}

PLUGIN_API void XPluginReceiveMessage(XPLMPluginID inFrom, int inMsg, void * inParam) {
}
