/* menu.c */

/*  This file is part of XReloadPlugins.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <stddef.h>
#include <string.h>

#include "XPLMMenus.h"

#include "main.h"
#include "menu.h"
#include "reload.h"


struct s_menu menu;

int menu_init(void) {
    // create menu container
    if ((menu.container_id = XPLMAppendMenuItem(XPLMFindPluginsMenu(), MENU_NAME_MAIN, 0, 0)) < 0) {
        return 1;
    }

    // create menu
    if ((menu.id = XPLMCreateMenu(MENU_NAME_MAIN, XPLMFindPluginsMenu(), menu.container_id, menu_handler, NULL)) == NULL) {
        return 2;
    }

    // create menu item
    if ((menu.settings_id = XPLMAppendMenuItem(menu.id, MENU_NAME_RELOAD, (void *)MENU_NAME_RELOAD, 1)) < 0) {
        return 3;
    }

    return 0;
}


void menu_destroy(void) {
    // destroy the menu
    if (menu.id)
        XPLMDestroyMenu(menu.id);
}


void menu_handler(void *in_menu_ref, void *in_item_ref) {
    // if the correct (only) menu is selected, call the actual reloading function
    if(!strcmp((const char *)in_item_ref, MENU_NAME_RELOAD)) {
        reload_plugins();
    }
}
